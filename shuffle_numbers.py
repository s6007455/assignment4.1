import sys

def shuffle(a,b):
	m = list(range(b,a-1,-1))
	return m

if __name__ == "__main__":
	a=int(sys.argv[1])
	b=int(sys.argv[2])
	print(shuffle(a,b))

